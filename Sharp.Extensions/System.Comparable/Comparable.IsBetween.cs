﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Checks if a value is between two others. Returns true if the value is between the borders and false if is not.
        /// </summary>
        /// <param name="value">The value to check</param>
        /// <param name="lower">The lower border</param>
        /// <param name="upper">The upper border</param>
        /// <param name="inclusive">Include the lower and upper borders in the comparison</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static bool IsBetween<T>(this T value, T lower, T upper, bool inclusive = false)
            where T : IComparable, IComparable<T>
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));
            if (lower == null)
                throw new ArgumentNullException(nameof(lower));
            if (upper == null)
                throw new ArgumentNullException(nameof(upper));

            return inclusive
                ? value.CompareTo(lower) >= 0 && value.CompareTo(upper) <= 0
                : value.CompareTo(lower) > 0 && value.CompareTo(upper) < 0;
        }
    }
}
