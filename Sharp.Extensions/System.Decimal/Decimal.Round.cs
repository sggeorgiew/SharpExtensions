﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Rounds a <see cref="decimal" /> value to the nearest integral value.
        /// </summary>
        /// <param name="value">A <see cref="decimal" /> value to be rounded</param>
        /// <exception cref="OverflowException"></exception>
        public static decimal Round(this decimal value)
        {
            return Math.Round(value);
        }

        /// <summary>
        /// Rounds a <see cref="decimal" /> value to a specified number of fractional digits.
        /// </summary>
        /// <param name="value">A <see cref="decimal" /> value to be rounded</param>
        /// <param name="decimals">The number of <see cref="decimal" /> places in the return value</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="OverflowException"></exception>
        public static decimal Round(this decimal value, int decimals)
        {
            return Math.Round(value, decimals);
        }

        /// <summary>
        /// Rounds a <see cref="decimal" /> value to the nearest integer. 
        /// A parameter specifies how to round the value if it is midway between two numbers.
        /// </summary>
        /// <param name="value">A <see cref="decimal" /> value to be rounded</param>
        /// <param name="mode">Specification for how to round the value if it is midway between two other numbers</param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="OverflowException"></exception>
        public static decimal Round(this decimal value, MidpointRounding mode)
        {
            return Math.Round(value, mode);
        }

        /// <summary>
        /// Rounds a <see cref="decimal" /> value to a specified number of fractional digits. 
        /// A parameter specifies how to round the value if it is midway between two numbers.
        /// </summary>
        /// <param name="value">A <see cref="decimal" /> value to be rounded</param>
        /// <param name="decimals">The number of <see cref="decimal" /> places in the return value</param>
        /// <param name="mode">Specification for how to round the value if it is midway between two other numbers</param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="OverflowException"></exception>
        public static decimal Round(this decimal value, int decimals, MidpointRounding mode)
        {
            return Math.Round(value, decimals, mode);
        }
    }
}
