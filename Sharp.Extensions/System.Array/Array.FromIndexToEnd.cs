﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Returns a sub-section of the current array, starting at the specified index and continuing to the end of the array.
        /// </summary>
        /// <param name="array">The <see cref="Array" /> with items</param>
        /// <param name="start">The index from which to start</param>
        public static T[] FromIndexToEnd<T>(this T[] array, int start)
        {
            var subSection = new T[array.Length - start];
            array.CopyTo(subSection, start);

            return subSection;
        }
    }
}
