﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Searches for the specified object in the array. Returns the first index or if the target cannot be found, returns -1.
        /// </summary>
        /// <param name="array">The <see cref="Array" /> with items</param>
        /// <param name="target">The <see cref="object" /> on which the index is searched</param>
        public static int IndexOf<T>(this Array array, T target)
        {
            for (var i = 0; i < array.Length; ++i)
            {
                if (array.GetValue(i).Equals(target))
                    return i;
            }

            return -1;
        }
    }
}
