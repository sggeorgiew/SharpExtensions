﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Clears all items in the <see cref="Array" />.
        /// </summary>
        /// <param name="array">The <see cref="Array" /> to clear</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void Clear(this Array array)
        {
            Array.Clear(array, 0, array.Length);
        }
    }
}
