﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Returns a sequence in random order.
        /// </summary>
        /// <param name="enumerable">The items collection</param>
        /// <param name="rand">A <see cref="Random" /> generator</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static IOrderedEnumerable<object> Randomize(this IEnumerable enumerable, Random rand = null)
        {
            return Randomize(enumerable.Cast<object>(), rand);
        }

        /// <summary>
        /// Returns a sequence in random order.
        /// </summary>
        /// <param name="enumerable">The items collection</param>
        /// <param name="rand">A <see cref="Random" /> generator</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static IOrderedEnumerable<T> Randomize<T>(this IEnumerable<T> enumerable, Random rand = null)
        {
            if (enumerable == null)
                throw new ArgumentNullException(nameof(enumerable));

            rand = rand ?? new Random();

            return enumerable.OrderBy(x => rand.NextDouble());
        }
    }
}
