﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Returns a random item of a sequence.
        /// </summary>
        /// <param name="enumerable">The items collection</param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public static T GetRandom<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
                throw new ArgumentNullException(nameof(enumerable));

            var list = enumerable as IList<T> ?? enumerable.ToArray();

            if (!list.Any())
                throw new ArgumentException("No items in collection");

            return list.Count <= 1
                ? list.First()
                : list[new Random().Next(0, list.Count)];
        }

        /// <summary>
        /// Returns a random item of a sequence or default value if the sequence is empty.
        /// </summary>
        /// <param name="enumerable">The items collection</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static T GetRandomOrDefault<T>(this IEnumerable<T> enumerable, T defaultValue = default(T))
        {
            if (enumerable == null)
                throw new ArgumentNullException(nameof(enumerable));

            var list = enumerable as IList<T> ?? enumerable.ToArray();

            return list.Any()
                ? GetRandom(list)
                : defaultValue;
        }
    }
}
