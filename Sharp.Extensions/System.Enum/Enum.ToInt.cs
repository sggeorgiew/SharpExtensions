﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts an <see cref="Enum" /> element to his <see cref="int" /> value.
        /// </summary>
        /// <param name="enumeration">The <see cref="Enum" /> value to convert</param>
        public static int ToInt(this Enum enumeration)
        {
            return Convert.ToInt32(enumeration);
        }
    }
}
