﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Determines whether the <see cref="string" /> is neither null, empty or whitespace.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to check</param>
        public static bool IsNotBlank(this string value)
        {
            return !string.IsNullOrWhiteSpace(value);
        }
    }
}
