﻿using System;
using System.Text.RegularExpressions;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Determines whether the <see cref="string" /> is numeric.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to check</param>
        public static bool IsNumeric(this string value)
        {
            return !Regex.IsMatch(value, "[^0-9]");
        }
    }
}
