﻿using System;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Returns a <see cref="string" /> that begins with a capital letter.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to capitalize</param>
        /// <exception cref="ArgumentException"></exception>
        public static string Capitalize(this string value)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
                throw new ArgumentException(nameof(value));

            return value.Substring(0, 1).ToUpper() + value.Substring(1);
        }
    }
}
