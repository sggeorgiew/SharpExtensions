﻿using System;
using System.Globalization;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Converts the <see cref="string" /> to a title case.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to convert</param>
        /// <param name="cultureInfo">The <see cref="CultureInfo" /> to use while converting</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="CultureNotFoundException"></exception>
        public static string ToTitleCase(this string value, CultureInfo cultureInfo = null)
        {
            return (cultureInfo ?? new CultureInfo("en-US")).TextInfo.ToTitleCase(value);
        }
    }
}
