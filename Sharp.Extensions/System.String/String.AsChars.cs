﻿using System;
using System.Collections.Generic;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Returns a <see cref="string" /> as chars.
        /// </summary>
        /// <param name="value">The <see cref="string" /> value to return as chars</param>
        public static IEnumerable<char> AsChars(this string value)
        {
            return value ?? null;
        }
    }
}
