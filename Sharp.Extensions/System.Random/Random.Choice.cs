﻿using System;
using System.Collections.Generic;

namespace Sharp.Extensions
{
    public static partial class Ext
    {
        /// <summary>
        /// Returns a random item chosen from the selected list.
        /// </summary>
        /// <param name="random">A <see cref="Random" /> generator</param>
        /// <param name="list">The list with items</param>
        public static T Choice<T>(this Random random, IReadOnlyList<T> list)
        {
            return list[random.Next(list.Count)];
        }
    }
}
